﻿using UnityEngine;
using System.Collections;


public class PlayerInput : MonoBehaviour
{

    public enum PlayerNumber
    {

        One,
        Two,
        Three,
        Four

    }

    public PlayerNumber myplayNumber;

    PlayerController _playerControlScript;


    // Use this for initialization
    void Start()
    {
        _playerControlScript = GetComponent<PlayerController>();


    }

    // Update is called once per frame
    void Update()
    {




        if (myplayNumber == PlayerNumber.One)
        {
            _playerControlScript.MoveH = Input.GetAxisRaw("Horizontal1");
            _playerControlScript.MoveV = Input.GetAxisRaw("Vertical1");
            _playerControlScript.inputA = Input.GetButton("Abutton1");
            _playerControlScript.inputY = Input.GetButton("Ybutton1");
            _playerControlScript.inputX = Input.GetButton("Xbutton1");
            _playerControlScript.inputB = Input.GetButton("Bbutton1");
            _playerControlScript.inputRun = Input.GetAxisRaw("Run1");
            //		
        }
        else if (myplayNumber == PlayerNumber.Two)
        {
            _playerControlScript.MoveH = Input.GetAxisRaw("Horizontal2");
            _playerControlScript.MoveV = Input.GetAxisRaw("Vertical2");
            _playerControlScript.inputA = Input.GetButton("Abutton2");
            _playerControlScript.inputY = Input.GetButton("Ybutton2");
            _playerControlScript.inputX = Input.GetButton("Xbutton2");
            _playerControlScript.inputB = Input.GetButton("Bbutton2");
            _playerControlScript.inputRun = Input.GetAxisRaw("Run2");

        }

        else if (myplayNumber == PlayerNumber.Three)
        {
            _playerControlScript.MoveH = Input.GetAxisRaw("Horizontal3");
            _playerControlScript.MoveV = Input.GetAxisRaw("Vertical3");
            _playerControlScript.inputA = Input.GetButton("Abutton3");
            _playerControlScript.inputY = Input.GetButton("Ybutton3");
            _playerControlScript.inputX = Input.GetButton("Xbutton3");
            _playerControlScript.inputB = Input.GetButton("Bbutton3");
            _playerControlScript.inputRun = Input.GetAxisRaw("Run3");

        }
        else if (myplayNumber == PlayerNumber.Four)
        {
            _playerControlScript.MoveH = Input.GetAxisRaw("Horizontal4");
            _playerControlScript.MoveV = Input.GetAxisRaw("Vertical4");
            _playerControlScript.inputA = Input.GetButton("Abutton4");
            _playerControlScript.inputY = Input.GetButton("Ybutton4");
            _playerControlScript.inputX = Input.GetButton("Xbutton4");
            _playerControlScript.inputB = Input.GetButton("Bbutton4");
            _playerControlScript.inputRun = Input.GetAxisRaw("Run4");

        }
    }
}
