﻿using UnityEngine;
using System.Collections;

public class PlayerFlicker : MonoBehaviour {

    public GameObject _playerModel;
    float _maxFlickerTime;
    float _currentFlickerTime;

    bool _playerVisable;
    bool _flicker;
    float flickerDuration;

	// Use this for initialization
	void Start () {
        _playerVisable = true;
        _currentFlickerTime = 0;
        _maxFlickerTime = 2f;
        _flicker = true;
        flickerDuration = 0.3f;
    }
	
	// Update is called once per frame
	void Update () {
        
            if (_flicker)
        {
            if (_currentFlickerTime >= _maxFlickerTime)
            {
                _flicker = false;
                _playerVisable = true;
                _playerModel.SetActive(true);
            }
            else
            {
                _currentFlickerTime += Time.deltaTime;
                if ((_currentFlickerTime% flickerDuration) > (flickerDuration/2) && _playerVisable)
                {
                    _playerModel.SetActive(false);
                    _playerVisable = false;
                }
                if ((_currentFlickerTime % flickerDuration) < (flickerDuration / 2) && !_playerVisable)
                {
                    _playerModel.SetActive(true);
                    _playerVisable = true;
                }
            }
        }
    }
    public void StartFlicker()
    {
        _flicker = true;
        _currentFlickerTime = 0;
        _maxFlickerTime = 1f;

    }
}
