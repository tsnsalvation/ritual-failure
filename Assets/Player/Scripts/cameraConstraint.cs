﻿using UnityEngine;
using System.Collections;

public class cameraConstraint : MonoBehaviour
{
    public Vector3 maxCons;
    Vector3 totalplayPos;
    public Vector3 playAverage;
    public GameObject[] players;
    public bool playmove;
    public Vector3 offset;
    float cameraLerpSpeed = 5;


    // Use this for initialization
    void Start()
    {

        players = GameObject.FindGameObjectsWithTag("Player");

        offset = this.transform.position - Vector3.up;

    }



    void FixedUpdate()
    {
        playAverage = Vector3.zero;
        //		players.Length[1]
        var playerLength = 0;
        foreach (GameObject player in players)
        {
            if (player.activeInHierarchy)
            {
                playAverage += player.transform.position;
                playerLength++;
            }
        }
        if (playerLength != 0)
            playAverage = (playAverage / playerLength);
        else
            playAverage = Vector3.down;
    }

    void LateUpdate()
    {
        if (playAverage == Vector3.down)
            return;
        Vector3 newposition = playAverage + offset;

        if (this.transform.position != newposition)
        {
            this.transform.position = Vector3.Lerp(transform.position, new Vector3(newposition.x, offset.y, newposition.z), Time.deltaTime * cameraLerpSpeed); //lerp
            //this.transform.position = newposition.y;
        }

    }

}
