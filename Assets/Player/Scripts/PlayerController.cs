﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    [Header("Input")]
    public float MoveH;
    public float MoveV;
    public bool inputA;
    public bool inputB;
    public bool inputX;
    public bool inputY;
    public float inputRun;
    [Header("Variables")]
    float speed;
    public float walkSpeed = 3;
    public float RunSpeed = 8;
    Rigidbody rb;
    public Camera cam;

    Vector3 targetDirection;
    Quaternion targetRotation;

    bool playedSound;

    public AudioSource runningPlayerSound;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cam = Camera.main;
        playedSound = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (inputRun >= 0.01)
        {
            if (!playedSound && !runningPlayerSound.isPlaying)
            {
                playedSound = true;
                runningPlayerSound.Play();
            }
            speed = RunSpeed;
        }
        else
        {
            
            playedSound = false;
            speed = walkSpeed;
        }
        Move();
        LimitPlayer();
    }



    void LimitPlayer()
    {
        float dist = (transform.position.y - Camera.main.transform.position.y);
        float leftLimitation = cam.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
        float rightLimitation = cam.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;

        float upLimitation = cam.ViewportToWorldPoint(new Vector3(0, 0, dist)).z;
        float downLimitation = cam.ViewportToWorldPoint(new Vector3(0, 1, dist)).z;

        upLimitation += 18.6f;
        downLimitation += 34.2f;
        leftLimitation += 1f;
        rightLimitation -= 1f;

        float tempx = Mathf.Clamp(transform.position.x, leftLimitation, rightLimitation);
        float tempz = Mathf.Clamp(transform.position.z, upLimitation, downLimitation);
        transform.position = new Vector3(tempx, 1, tempz);
    }

    void Move()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        targetDirection = new Vector3(MoveH, 0f, MoveV);
        //targetDirection = cam.transform.TransformDirection(targetDirection.normalized);

        targetDirection = targetDirection.normalized;
        targetDirection.y = 0.0f;
        if (targetDirection != Vector3.zero)
        {
            targetRotation = Quaternion.LookRotation(targetDirection);
            rb.MoveRotation(targetRotation);
        }
        targetDirection *= speed;

        rb.MovePosition(transform.position + targetDirection * Time.deltaTime);


    }
}
