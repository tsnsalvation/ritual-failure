﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{
    ScoreManager _scorescript;
    public Image Healthbar;
    public float maxHealth;
    public float currentHealth;
    public float healthUpdate
    {
        get { return currentHealth; }
        set
        {
            currentHealth = value;
            changeHealthBar();
            if (currentHealth <= 0)
                OnDeath();

        }
    }


    PlayerFlicker _flicker;
    public bool dead;

    public PlayerSpawner mainPlayerSpawner;
    int playerNumber;
    public int deathScoreValue = -100;


    // Use this for initialization
    void Start()
    {
        _flicker = GetComponent<PlayerFlicker>();
        currentHealth = 100f;
        maxHealth = 100f;


        if (this.transform.name == "Player1")
        {
            playerNumber = 1;
        }
        if (this.transform.name == "Player2")
        {
            playerNumber = 2;
        }

        if (this.transform.name == "Player3")
        {
            playerNumber = 3;
        }
        if (this.transform.name == "Player4")
        {
            playerNumber = 4;
        }
        _scorescript = GameObject.Find("Score").GetComponent<ScoreManager>();


    }


    void changeHealthBar()
    {
        Healthbar.fillAmount = currentHealth / maxHealth;
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.transform.tag == "Enemy")
        {
            _flicker.StartFlicker();
            healthUpdate -= 10;
            changeHealthBar();
            transform.position = transform.position + (transform.position - other.transform.position);
        }

    }

    void OnDeath()
    {
        if (dead)
            return;
        this.gameObject.SetActive(false);
        _scorescript.addToScore(deathScoreValue);
        mainPlayerSpawner.PlayerDeath(playerNumber);
        dead = true;
    }
}
