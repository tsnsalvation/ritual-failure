﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuControl : MonoBehaviour {

	public void OnStartGame()
    {
        SceneManager.LoadScene(1);
    }
    public void OnExit()
    {
        Application.Quit();
    }
    public void OnCreditsClick()
    {

    }
    public void OnHighscoresClick()
    {

    }

}
