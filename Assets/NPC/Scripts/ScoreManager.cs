﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {
	public float displayScore;
	public float CurrentScore;
	public float NextScore;
	public Text ScoreText;
	public float scorecountspeed = 1;
	public float counDuration = 2f;
	// Use this for initialization
	void Start () {
		ScoreText = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		scorecountspeed += Time.deltaTime;
		float counspedd = scorecountspeed / counDuration;


		displayScore = Mathf.Round(Mathf.Lerp (displayScore, NextScore, counspedd));
		ScoreText.text = "Score: " + displayScore.ToString ();
	}

	public void addToScore(int amount)
	{
		CurrentScore = displayScore;
		NextScore += amount;

		scorecountspeed = 0;
	}
}
