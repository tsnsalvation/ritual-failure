﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Grid
{

    public LayerMask unwalkableMask = 1 << 9;
    public Vector3 gridWorldSize;
    public float nodeRadius = 0.5f;
    public Node[,] grid;

    Vector3 gridBottomLeft;

    public bool displayGridGizmos = false;

    float nodeDiameter;
    int gridSizeX, gridSizeZ;
    
    public Grid(Vector3 worldSize, Vector3 bottomLeft)
    {
        gridWorldSize = worldSize;
        gridBottomLeft = bottomLeft;
        Init();

    }
    
    void Init()
    {
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeZ = Mathf.RoundToInt(gridWorldSize.z / nodeDiameter);

        CreateGrid(gridBottomLeft);


    }
    public int MaxSize
    {
        get
        {
            return gridSizeX * gridSizeZ;
        }
    }

    void CreateGrid(Vector3 worldBottomLeft)
    {
        try
        {
            grid = new Node[gridSizeX, gridSizeZ];
        }
        catch
        {
            Debug.Log("i");
        }

        for (int x = 0; x < gridSizeX; x++)
        {
            for (int z = 0; z < gridSizeZ; z++)
            {
                Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.up * (nodeDiameter + nodeRadius) + Vector3.forward * (z * nodeDiameter + nodeRadius);
                bool walkable = !(Physics.CheckSphere(worldPoint, nodeRadius, unwalkableMask));
                grid[x, z] = new Node(walkable, worldPoint, x, z);
            }
        }

    }

    public Node NodeFromWorldPoint(Vector3 worldPos)
    {
        float percentX = ((worldPos.x - gridBottomLeft.x) / gridWorldSize.x);
        float percentZ = ((worldPos.z - gridBottomLeft.z) / gridWorldSize.z);
        percentX = Mathf.Clamp01(percentX);
        percentZ = Mathf.Clamp01(percentZ);

        int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
        int z = Mathf.RoundToInt((gridSizeZ - 1) * percentZ);

        return grid[x, z];

    }

    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();
        for (int x = -1; x <= 1; x++)
        {
            for (int z = -1; z <= 1; z++)
            {
                if (x == 0 && z == 0)
                {
                    continue;
                }
                int checkX = node.gridX + x;
                int checkZ = node.gridZ + z;

                if (checkX >= 0 && checkX < gridSizeX && checkZ >= 0 && checkZ < gridSizeZ)
                {
                    neighbours.Add(grid[checkX, checkZ]);
                }

            }

        }
        return neighbours;
    }

}
