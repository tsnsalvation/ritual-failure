﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]
public class EnemyAI : MonoBehaviour
{

    Vector3 targetLocation;

    AStarPathFinding pathFinder;
    DynamicPathFinder gridFinder;

    public int state;
    public float patrolVariance = 10.0f;
    float waitTime = 3f;
    float currentWait = 0.0f;

    public List<Vector3> pathFindLocations;

    Rigidbody rb;

    float proximityVariance = 0.5f;

    public bool hasPath = false;
    public bool waitingForPath = false;

    public float speed;
    // Use this for initialization
    void Start()
    {
        pathFinder = this.GetComponent<AStarPathFinding>();
        rb = GetComponent<Rigidbody>();
        gridFinder = GetComponent<DynamicPathFinder>();
        state = 1;
        speed = 1;
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        if (state == 1) //idle
        {
            if (currentWait > waitTime)
            {
                if (!hasPath && !waitingForPath)
                {
                    targetLocation = GetNearPoint();
                    gridFinder.FindPath(transform.position, targetLocation);
                    pathFinder.grid = new Grid(gridFinder.gridSize, gridFinder.gridStart);
                    hasPath = false;
                    waitingForPath = true;
                    pathFinder.StartFindPath(transform.position, targetLocation);
                    state = 2;
                    currentWait = 0;
                }
                if (hasPath)
                {
                    state = 2;
                }
            }
            else
            {
                currentWait += Time.deltaTime;
            }
        }
        if (state == 2) //patrol
        {
            if (waitingForPath)
                return;
            MoveThis();
        }
    }
    public void OnPathFound(List<Vector3> path, bool success)
    {
        if (success)
        {
            pathFindLocations = path;
            waitingForPath = false;
            hasPath = true;
        }
    }

    void MoveThis()
    {

        if (pathFindLocations != null && pathFindLocations.Count != 0)
        {
            if (CloseToWaypoint(transform.position, pathFindLocations[0]))
            {
                pathFindLocations.RemoveAt(0);


            }
            if (pathFindLocations.Count == 0)
            {
                hasPath = false;
                state = 1;
                return;
            }
            Vector3 travelDirection = pathFindLocations[0] - transform.position;
            
            travelDirection = travelDirection.normalized;
            travelDirection.y = 0;
            rb.MovePosition(transform.position + travelDirection * Time.deltaTime * speed);
            if (travelDirection != Vector3.zero)
            {
                
                rb.MoveRotation(Quaternion.LookRotation(travelDirection));
            }

        }
        if (pathFindLocations.Count == 0)
        {
            hasPath = false;
            state = 1;
            return;
        }
    }

    Vector3 GetNearPoint()
    {

        float x = Random.Range(transform.position.x - patrolVariance, transform.position.x + patrolVariance);
        float z = Random.Range(transform.position.z - patrolVariance, transform.position.z + patrolVariance);


        return new Vector3(x, 1, z);
    }
    bool CloseToWaypoint(Vector3 pos, Vector3 tar)
    {
        if (pos.x > tar.x - proximityVariance && pos.x < tar.x + proximityVariance &&
            pos.z > tar.z - proximityVariance && pos.z < tar.z + proximityVariance)
        {
            return true;
        }
        return false;
    }
}
