﻿using UnityEngine;
using System.Collections;

public class NPCHealth : MonoBehaviour
{
    public ScoreManager _scoreScript;
    public float maxHealth = 100;
    public float currentHealth;
    public int scoreValue;



    public virtual  void TakeDamage(int amount)
    {
        currentHealth -= amount;

    }

    public virtual void OnDeath()
    {
        _scoreScript.addToScore(scoreValue);

    }
}