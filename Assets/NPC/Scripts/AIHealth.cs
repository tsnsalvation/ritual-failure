﻿
using UnityEngine;
using System.Collections;

public class AIHealth : NPCHealth
{



    public void Start()
    {
        currentHealth = maxHealth;
        _scoreScript = GameObject.Find("Score").GetComponent<ScoreManager>();
        scoreValue = 100;
    }
    public override void TakeDamage(int value)
    {
        base.TakeDamage(value);
        if (currentHealth <= 0)
            OnDeath();
    }

    public override void OnDeath()
    {
        base.OnDeath();
        currentHealth = maxHealth;
        gameObject.SetActive(false);
    }
}
