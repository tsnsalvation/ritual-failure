﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(AStarPathFindingAggressive))]
[RequireComponent(typeof(DynamicPathFinder))]
[RequireComponent(typeof(Rigidbody))]

public class AgressiveEnemyAI : MonoBehaviour
{

    Vector3 targetLocation;

    AStarPathFindingAggressive pathFinder;
    DynamicPathFinder gridFinder;

    public int state;
    public float patrolVariance = 10.0f;
    float waitTime = 1f;
    float currentWait = 0.0f;

    public List<Vector3> pathFindLocations;

    Rigidbody rb;

    float proximityVariance = 0.5f;

    public bool hasPath = false;
    public bool waitingForPath = false;

    public float speed;

    public int thisSummonAttitude;

    public GameObject huntedTarget;

    float huntRange = 15;
    float attackRange = 1.6f;
    float attackTimer;
    float maxAttackTimer = 1f;

    public Animator anim;

    public int damageAmount;

    // Use this for initialization
    void Start()
    {
        pathFinder = this.GetComponent<AStarPathFindingAggressive>();
        rb = GetComponent<Rigidbody>();
        gridFinder = GetComponent<DynamicPathFinder>();
        
        state = 3;

    }

    // Update is called once per frame
    void Update()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Attacking") && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >=0.5f )
        {
            anim.SetBool("Attacking", false);
        }
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        if (state == 1) //idle
        {
            if (currentWait > waitTime)
            {

                state = 3;
            }
            else
            {
                currentWait += Time.deltaTime;
            }
        }
        if (state == 2) //patrol
        {
            if (waitingForPath)
                return;
            MoveThis();
        }
        if (state == 3) //hunting
        {
            if (huntedTarget != null)
            {
                //is target in range?
                if (Vector3.Distance(new Vector3(transform.position.x, 0, transform.position.z), new Vector3(huntedTarget.transform.position.x, 0, huntedTarget.transform.position.z)) < attackRange)
                {
                    if (attackTimer >= maxAttackTimer)
                    {
                        anim.SetBool("Attacking", true);
                        if (huntedTarget.tag == "Player")
                            huntedTarget.GetComponent<PlayerHealth>().healthUpdate -= damageAmount;

                        attackTimer = 0.0f;
                    }
                    else
                    {
                        attackTimer += Time.deltaTime;
                    }
                }
                else
                {
                    if (hasPath)
                    {
                        MoveThis();
                    }
                    else
                    {
                        //targetLocation = huntedTarget.transform.position;
                        gridFinder.FindPath(transform.position, huntedTarget.transform.position);
                        pathFinder.grid = new Grid(gridFinder.gridSize, gridFinder.gridStart);
                        hasPath = false;
                        waitingForPath = true;
                        pathFinder.StartFindPath(transform.position, huntedTarget.transform.position);
                        //state = 2;
                        currentWait = 0;
                    }
                }
                if (Random.Range(0f, 1000f) > 998)
                {

                    huntedTarget = null;
                }
            }
            else
            {
                FindTarget();
            }
        }
    }
    public void OnPathFound(List<Vector3> path, bool success)
    {
        if (success)
        {
            pathFindLocations = path;
            waitingForPath = false;
            hasPath = true;
        }
        else
        {
            waitingForPath = false;
        }
    }

    void MoveThis()
    {
        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Move"))
        {
            anim.SetBool("Moving", true);
        }

        if (pathFindLocations != null && pathFindLocations.Count != 0)
        {
            if (CloseToWaypoint(transform.position, pathFindLocations[0]))
            {
                pathFindLocations.RemoveAt(0);


            }
            if (pathFindLocations.Count == 0)
            {
                hasPath = false;
                //state = 1;
                return;
            }
            Vector3 travelDirection = pathFindLocations[0] - transform.position;

            travelDirection = travelDirection.normalized;
            travelDirection.y = 0;
            rb.MovePosition(transform.position + travelDirection * Time.deltaTime * speed);
            if (travelDirection != Vector3.zero)
            {

                rb.MoveRotation(Quaternion.LookRotation(travelDirection));
            }

        }
        if (pathFindLocations.Count == 0)
        {
            hasPath = false;
            //state = 1;
            return;
        }
    }


    Vector3 GetNearPoint()
    {

        float x = Random.Range(transform.position.x - patrolVariance, transform.position.x + patrolVariance);
        float z = Random.Range(transform.position.z - patrolVariance, transform.position.z + patrolVariance);


        return new Vector3(x, 1, z);
    }
    bool CloseToWaypoint(Vector3 pos, Vector3 tar)
    {
        if (pos.x > tar.x - proximityVariance && pos.x < tar.x + proximityVariance &&
            pos.z > tar.z - proximityVariance && pos.z < tar.z + proximityVariance)
        {
            return true;
        }
        return false;
    }

    void FindTarget()
    {

        GameObject[] players;
        players = GameObject.FindGameObjectsWithTag("Player");

        for (int i = 0; i < players.Length; i++)
        {

            var attack = Random.Range(0, 2);
            if (attack >= 1)
            {
                huntedTarget = players[i];
                break;
            }

        }

        attackTimer = maxAttackTimer;
    }
}
