﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Monster_Controller : MonoBehaviour {
	public GameObject[] PartHolders;

	public GameObject head;
	public GameObject legs;
	public GameObject arms;
	public GameObject chest;
	public string monsterName;
	public Animator[] anims;
	SummonAI _aiscript;
	// 0 is Head Point
	// 1 is Leg Point
	// 2 is Arm Point
	// 3 is Chest Point
	// Use this for initialization
	void Start () 
	{
		_aiscript = this.gameObject.GetComponent<SummonAI>();
	}

	 void Update()
	{
		if (anims.Length == 4) 
		{
			if (_aiscript.hasPath) 
			{
				foreach (Animator anim in anims)
				{
					anim.SetBool ("Moving", true);
				}

			}
			else 
			{
				foreach (Animator anim in anims)
				{
					anim.SetBool ("Moving", false);
				}
			}
		}
	}

	public void UpdateStats()
	{
	//	monsterName = (head.GetComponent<Part_stats> ().NameMod +"-"+ arms.GetComponent<Part_stats> ().NameMod + "-"+ chest.GetComponent<Part_stats> ().NameMod +"-"+ legs.GetComponent<Part_stats> ().NameMod);
	//	GameObject.Find ("MonsterName").GetComponent<Text> ().text = monsterName;
	//	GameObject.Find ("MonsterName").GetComponent<Animation> ().Play ();
	}

	public void PartsToSpawn(GameObject Head, GameObject Chest, GameObject Arms, GameObject Legs)
	{
		anims = new Animator[4];
		(head = Instantiate (Head, PartHolders [0].transform.position, PartHolders [0].transform.rotation)as GameObject).transform.parent = PartHolders [0].transform ;
		(legs = Instantiate (Legs, PartHolders [1].transform.position, PartHolders [1].transform.rotation)as GameObject).transform.parent = PartHolders [1].transform;
		(arms = Instantiate (Arms, PartHolders [2].transform.position, PartHolders [2].transform.rotation)as GameObject).transform.parent = PartHolders [2].transform;
		(chest = Instantiate (Chest, PartHolders [3].transform.position, PartHolders [3].transform.rotation)as GameObject).transform.parent = PartHolders [3].transform;
		anims [0] = head.GetComponent<Animator> ();
		anims [1] = legs.GetComponent<Animator> ();
		anims [2] = arms.GetComponent<Animator> ();
		anims [3] = chest.GetComponent<Animator> ();
        //UpdateStats ();
    }
}
