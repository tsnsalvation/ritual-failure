﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class AStarPathFinding : MonoBehaviour
{

    //PathRequestManager requestManager;
    EnemyAI thisEnemy;
    public Grid grid;

    public void StartFindPath(Vector3 start, Vector3 end)
    {
        thisEnemy = GetComponent<EnemyAI>();
        if (grid != null)
        {
            StartCoroutine(FindPath(start, end));
        }
    }



    IEnumerator FindPath(Vector3 startPos, Vector3 finishPos)
    {

        List<Vector3> waypoints = new List<Vector3>();
        bool pathSuccess = false;

        Node startNode = grid.NodeFromWorldPoint(startPos);
        Node finishNode = grid.NodeFromWorldPoint(finishPos);

        Heap<Node> openSet = new Heap<Node>(grid.MaxSize);
        HashSet<Node> closedSet = new HashSet<Node>();
        openSet.Add(startNode);

        while (openSet.Count > 0)
        {
            
            Node currentNode = openSet.RemoveFirst();

            closedSet.Add(currentNode);

            if (currentNode == finishNode)
            {

                //print("Path :" + sw.ElapsedMilliseconds + "ms");
                pathSuccess = true;
                break;
            }
            
            //print("Current Node: " + currentNode.gridX + "," + currentNode.gridY + "," + currentNode.gridZ);
            foreach (Node neighbour in grid.GetNeighbours(currentNode)) //Start thinking here for one way nodes
            {
                
                //print("N: " + neighbour.gridX +","+neighbour.gridY +","+neighbour.gridZ + " " + neighbourTravelsableBit + " vs " + currentNode.travelDirection);
                if (!neighbour.walkable || closedSet.Contains(neighbour) )//||
                //    (usesGravity && (!grid.GroundBelow(neighbour)) && neighbour.gridY >= currentNode.gridY)) //checking the bitwise for traversable ( || (neighbourTravelsableBit & currentNode.travelDirection) > 0 )
                {
                    continue;
                }
                int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);
                if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                {

                    neighbour.gCost = newMovementCostToNeighbour;
                    neighbour.hCost = GetDistance(neighbour, finishNode);
                    neighbour.parent = currentNode;

                    if (!openSet.Contains(neighbour))
                    {
                        openSet.Add(neighbour);
                    }
                    else
                    {
                        openSet.UpdateItem(neighbour);
                    }
                }

            }
            
        }


        yield return null;
        if (pathSuccess)
        {
            waypoints = RetracePath(startNode, finishNode);

        }
        //requestManager.FinishedProcessingPath(waypoints, pathSuccess);
        thisEnemy.OnPathFound(waypoints, pathSuccess);

    }
    List<Vector3> RetracePath(Node startNode, Node endNode)
    {

        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        List<Vector3> waypoints = SimplifyPath(path);
        waypoints.Reverse(); ;
        return waypoints;

    }
    List<Vector3> SimplifyPath(List<Node> path)
    {
        List<Vector3> waypoints = new List<Vector3>();
        Vector3 directionOld = Vector3.zero;

        for (int i = 1; i < path.Count; i++)
        {
            Vector3 directionNew = new Vector3(path[i - 1].gridX - path[i].gridX,1, path[i - 1].gridZ - path[i].gridZ);
            if (directionNew != directionOld)
            {
                waypoints.Add(ReturnWaypointAtGround(path[i].worldPos));
            }
            directionOld = directionNew;

        }
        return waypoints;
    }


    int GetDistance(Node nodeA, Node nodeB)
    {
        int distanceX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int distanceZ = Mathf.Abs(nodeA.gridZ - nodeB.gridZ);

        if (distanceX > distanceZ)
            return (14 * distanceZ) + (10 * (distanceX-distanceZ));
        return (14 * distanceX) + (10 * (distanceZ - distanceX));


    }

    Vector3 ReturnWaypointAtGround(Vector3 waypoint)
    {
        RaycastHit ray;

        if (Physics.Raycast(waypoint,Vector3.down, out ray))
        {
            return new Vector3(waypoint.x, ray.point.y, waypoint.z);
        }
        return waypoint;
    }

    void OnDrawGizmos()
    {
        //Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, gridWorldSize.y, gridWorldSize.z));

        if (grid != null)//&& displayGridGizmos)
        {

            foreach (Node n in grid.grid)
            {
                Gizmos.color = (n.walkable) ? Color.white : Color.red;
                Gizmos.DrawCube(n.worldPos, Vector3.one * (.8f));
            }
        }
    }
}
