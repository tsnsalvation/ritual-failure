﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(AStarPathFindingSummon))]
[RequireComponent(typeof(DynamicPathFinder))]
[RequireComponent(typeof(Rigidbody))]

public class SummonAI : MonoBehaviour
{

    Vector3 targetLocation;

    AStarPathFindingSummon pathFinder;
    DynamicPathFinder gridFinder;

    public int state;
    public float patrolVariance = 10.0f;
    float waitTime = 1f;
    float currentWait = 0.0f;

    public List<Vector3> pathFindLocations;

    Rigidbody rb;

    float proximityVariance = 0.5f;

    public bool hasPath = false;
    public bool waitingForPath = false;

    public float speed;

    public int thisSummonAttitude;

    public GameObject huntedTarget;

    float huntRange = 15;
    float attackRange = 2.6f;
    float attackTimer;
    float maxAttackTimer = 1f;


    // Use this for initialization
    void Start()
    {
        pathFinder = this.GetComponent<AStarPathFindingSummon>();
        rb = GetComponent<Rigidbody>();
        gridFinder = GetComponent<DynamicPathFinder>();
        state = 3;
        speed = 8;
    }

    // Update is called once per frame
    void Update()
    {
       
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        if (state == 1) //idle
        {
            if (currentWait > waitTime)
            {

                state = 3;
            }
            else
            {
                currentWait += Time.deltaTime;
            }
        }
        if (state == 2) //patrol
        {
            if (waitingForPath)
                return;
            MoveThis();
        }
        if (state == 3) //hunting
        {
            if (huntedTarget != null)
            {


                //is target in range?
                if (Vector3.Distance(new Vector3(transform.position.x, 0, transform.position.z), new Vector3(huntedTarget.transform.position.x, 0, huntedTarget.transform.position.z)) < attackRange)
                {
                    if (attackTimer >= maxAttackTimer)
                    {
                        if (huntedTarget.tag == "Player")
                            huntedTarget.GetComponent<PlayerHealth>().healthUpdate -= 20;
                        else if (huntedTarget.tag == "Enemy")
                            huntedTarget.GetComponent<AIHealth>().TakeDamage(40);
                        else
                            huntedTarget.GetComponent<SummonHealth>().TakeDamage(25);
                        attackTimer = 0.0f;
                    }
                    else
                    {
                        attackTimer += Time.deltaTime;
                    }
                }
                else
                {
                    if (hasPath)
                    {
                        MoveThis();
                    }
                    else
                    {
                        //targetLocation = huntedTarget.transform.position;
                        gridFinder.FindPath(transform.position, huntedTarget.transform.position);
                        pathFinder.grid = new Grid(gridFinder.gridSize, gridFinder.gridStart);
                        hasPath = false;
                        waitingForPath = true;
                        pathFinder.StartFindPath(transform.position, huntedTarget.transform.position);
                        //state = 2;
                        currentWait = 0;
                    }
                }
                if (Random.Range(0f, 1000f) > 998)
                {

                    huntedTarget = null;
                }
            }
            else
            {
                FindTarget();
            }
        }
    }
    public void OnPathFound(List<Vector3> path, bool success)
    {
        if (success)
        {
            pathFindLocations = path;
            waitingForPath = false;
            hasPath = true;
        }
        else
        {
            waitingForPath = false;
        }
    }

    void MoveThis()
    {

        if (pathFindLocations != null && pathFindLocations.Count != 0)
        {
            if (CloseToWaypoint(transform.position, pathFindLocations[0]))
            {
                pathFindLocations.RemoveAt(0);


            }
            if (pathFindLocations.Count == 0)
            {
                hasPath = false;
                //state = 1;
                return;
            }
            Vector3 travelDirection = pathFindLocations[0] - transform.position;

            travelDirection = travelDirection.normalized;
            travelDirection.y = 0;
            rb.MovePosition(transform.position + travelDirection * Time.deltaTime * speed);
            if (travelDirection != Vector3.zero)
            {

                rb.MoveRotation(Quaternion.LookRotation(travelDirection));
            }

        }
        if (pathFindLocations.Count == 0)
        {
            hasPath = false;
            //state = 1;
            return;
        }
    }


    Vector3 GetNearPoint()
    {

        float x = Random.Range(transform.position.x - patrolVariance, transform.position.x + patrolVariance);
        float z = Random.Range(transform.position.z - patrolVariance, transform.position.z + patrolVariance);


        return new Vector3(x, 1, z);
    }
    bool CloseToWaypoint(Vector3 pos, Vector3 tar)
    {
        if (pos.x > tar.x - proximityVariance && pos.x < tar.x + proximityVariance &&
            pos.z > tar.z - proximityVariance && pos.z < tar.z + proximityVariance)
        {
            return true;
        }
        return false;
    }

    void FindTarget()
    {
        if (thisSummonAttitude == 1)//good
        {
            GameObject[] slugs;
            GameObject[] summons;
            GameObject[] allEnemies;

            slugs = GameObject.FindGameObjectsWithTag("Enemy");
            summons = GameObject.FindGameObjectsWithTag("Summon");
            allEnemies = new GameObject[slugs.Length + summons.Length];
            //System.Array.Copy(slugs, allEnemies, 0);
            //System.Array.Copy(summons, 0, allEnemies, slugs.Length, summons.Length);
            slugs.CopyTo(allEnemies, 0);
            summons.CopyTo(allEnemies, slugs.Length);
            

            for (int i = 0; i < allEnemies.Length; i++)
            {
                if (Vector3.Distance(transform.position, allEnemies[i].transform.position) < huntRange)
                {
                    var attack = Random.Range(0, 2);
                    if (attack >= 1)
                    {
                        huntedTarget = allEnemies[i];
                        break;
                    }
                }
            }
        }
        if (thisSummonAttitude == 2)//bad
        {
            GameObject[] players;
            players = GameObject.FindGameObjectsWithTag("Player");

            for (int i = 0; i < players.Length; i++)
            {

                var attack = Random.Range(0, 2);
                if (attack >= 1)
                {
                    huntedTarget = players[i];
                    break;
                }

            }
        }
        attackTimer = maxAttackTimer;
    }
}
