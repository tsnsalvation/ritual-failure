﻿using UnityEngine;
using System.Collections;

public class DynamicPathFinder : MonoBehaviour {


    public int pathOffset = 5;

    public Vector3 gridStart, gridSize;

    public void  FindPath(Vector3 startPos, Vector3 endPos)
    {
        int x,y,z;
        float width,height,depth;


        width = endPos.x - startPos.x;
        if (width > 0 )
        {
            x = (int)(startPos.x - pathOffset);
        }
        else
        {
            x = (int)(endPos.x - pathOffset);
            width = Mathf.Abs(width);
        }
        width = Mathf.Round(width) + (pathOffset * 2);

        height = 1;

        depth = endPos.z - startPos.z;
        if (depth > 0)
        {
            z = (int)(startPos.z - pathOffset);
        }
        else
        {
            z = (int)(endPos.z - pathOffset);
            depth = Mathf.Abs(depth);
        }
        depth = Mathf.Round(depth) + (pathOffset * 2);

        gridStart = new Vector3 (x,-0.7f,z);
        gridSize = new Vector3 (width,height,depth);
    }
}
