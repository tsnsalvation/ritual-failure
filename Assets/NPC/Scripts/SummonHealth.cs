﻿using UnityEngine;
using System.Collections;

public class SummonHealth : NPCHealth
{

    public float maxSpawnTime;
    public float currentSpawnTime;


    void Start()
    {
        currentHealth = maxHealth;
        _scoreScript = GameObject.Find("Score").GetComponent<ScoreManager>();
        currentSpawnTime = 0;
        scoreValue = 100;

    }
    void Update()
    {
        if (currentSpawnTime > maxSpawnTime)
        {
            OnDeath();
        }
        else
        {
            currentSpawnTime += Time.deltaTime;
        }
    }
    public override void TakeDamage(int value)
    {
        base.TakeDamage(value);
        if (currentHealth <= 0)
        {
            scoreValue = 0;
            OnDeath();
        }
    }

    public override void OnDeath()
    {
        base.OnDeath();
        Destroy(this.gameObject);
    }
}
