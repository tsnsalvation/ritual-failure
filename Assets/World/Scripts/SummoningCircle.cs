﻿using UnityEngine;
using System.Collections;
public class SummoningCircle : MonoBehaviour {
	public bool ActiveCircle;
	public bool canActivate ;
//	public GameObject[] summoner;

	public PlayerController _playerscript;
	public SelectionPoint[] _pointscripts;
	// 0 is Head Point
	// 1 is Leg Point
	// 2 is Arm Point
	// 3 is Chest Point
	public GameObject monsterBase;
    
	public GameObject spawner;
	bool cantSpawnMonster;
	ScoreManager _scoreScript;

    public AudioSource summonSound;
    public ParticleSystem summonCircle;

    public ParticleSystem monsterSpawn;

    // Use this for initialization
    void Start () {
		_scoreScript = GameObject.Find ("Score").GetComponent<ScoreManager>();
        
    }
	
	// Update is called once per frame
	void Update () {

		if (_pointscripts [0].hasPlayer == true) 
		{
			canActivate = true	;
		}
		if (ActiveCircle) 
		{
			canActivate = false;
		}

		if(_pointscripts[0].partToSpawn != null && _pointscripts[1].partToSpawn != null && _pointscripts[2].partToSpawn != null && _pointscripts[3].partToSpawn != null)
		{
			
			if (!cantSpawnMonster) 
			{
				SpawnMonster ();
			}
		}

	}

	void SpawnMonster()
	{
        summonSound.Play();
        summonCircle.Stop();
        Debug.Log ("Spawn Monster");
        monsterSpawn.Play();
        Monster_Controller _monScript;
		GameObject monster;
		monster = (GameObject)Instantiate (monsterBase, spawner.transform.position, spawner.transform.rotation);
		_monScript = monster.GetComponent<Monster_Controller> ();
		_monScript.PartsToSpawn (_pointscripts [0].partToSpawn, _pointscripts [3].partToSpawn, _pointscripts [2].partToSpawn, _pointscripts [1].partToSpawn);
        monster.GetComponent<SummonAI>().thisSummonAttitude = Random.Range(1,3);
		_scoreScript.addToScore (300);
		cantSpawnMonster = true;
	}

	void OnTriggerExit(Collider col)
	{
		if (col.tag == "Player" && ActiveCircle) 
		{
			Failure ();
		}
	}

	void Failure()
	{
		ActiveCircle = false;
	}
}
