﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnController : MonoBehaviour
{
    public int slugCount;
    public int batCount;
    public int spiderCount;
    public int wolfCount;
    public int manticoreCount;

    public int maxSlugCount;
    public int maxBatCount;
    public int maxSpiderCount;
    public int maxWolfCount;
    public int maxManticoreCount;

    public GameObject slugPrefab;
    public GameObject batPrefab;
    public GameObject spiderPrefab;
    public GameObject wolfPrefab;
    public GameObject manticorePrefab;

    public List<GameObject> slugList = new List<GameObject>();
    public List<GameObject> batList = new List<GameObject>();
    public List<GameObject> spiderList = new List<GameObject>();
    public List<GameObject> wolfList = new List<GameObject>();
    public List<GameObject> manticoreList = new List<GameObject>();

    public enum spawnPhases
    {
        slug,
        bat,
        spider,
        wolf,
        manticore
    }

    public Transform caveInhabitantGroup;

    public float proximitySpawn = 30;
    public float maxDistanceForSpawn = 50;

    public float phaseTimer;

    float[] phaseTimeLimits = { 90f,90f,90f,90f,90f};



    public spawnPhases _spanwPhase;

    // Use this for initialization
    void Start()
    {
        _spanwPhase = spawnPhases.slug;
        InitialiseSlugs();
        InitialiseBats();
        InitialiseSpider();
    }

    void InitialiseSlugs()
    {
        for (int i = 0; i < maxSlugCount; i++)
        {
            GameObject go = Instantiate(slugPrefab);
            go.transform.parent = caveInhabitantGroup;
            slugList.Add(go);
            go.SetActive(false);
        }
    }

    void InitialiseBats()
    {
        for (int i = 0; i < maxBatCount; i++)
        {
            GameObject go = Instantiate(batPrefab);
            go.transform.parent = caveInhabitantGroup;
            batList.Add(go);
            go.SetActive(false);
        }
    }
    void InitialiseSpider()
    {
        for (int i = 0; i < maxSpiderCount; i++)
        {
            GameObject go = Instantiate(spiderPrefab);
            go.transform.parent = caveInhabitantGroup;
            spiderList.Add(go);
            go.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_spanwPhase == spawnPhases.slug)
        {
            if (Random.Range(0, 1000) > 985 && slugCount < maxSlugCount)
            {
                SpawnSlug(GetSpawnLoaction());
            }
        }
        if (_spanwPhase == spawnPhases.bat)
        {

            if (Random.Range(0, 1000) > 985 && batCount < maxBatCount)
            {

                SpawnBat(GetSpawnLoaction());
            }
        }
        if (_spanwPhase == spawnPhases.spider)
        {

            if (Random.Range(0, 1000) > 985 && spiderCount < maxSpiderCount)
            {

                SpawnSpider(GetSpawnLoaction());
            }
        }
        if (_spanwPhase == spawnPhases.wolf)
        {

            if (Random.Range(0, 1000) > 985 && wolfCount < maxWolfCount)
            {

                SpawnSlug(GetSpawnLoaction());
            }
        }
        if (_spanwPhase == spawnPhases.manticore)
        {

            if (Random.Range(0, 1000) > 985 && manticoreCount < maxManticoreCount)
            {

                SpawnSlug(GetSpawnLoaction());
            }
        }
        if (slugCount != 0)
        {
            DespawnSlug();
        }
        if (batCount != 0)
        {
            DespawnBat();
        }
        if (spiderCount != 0)
        {
             DespawnSpider();
        }
        if (wolfCount != 0)
        {
            //DespawnWolf();
        }
        if (manticoreCount != 0)
        {
            //DespawnManticore();
        }
        if (phaseTimer > phaseTimeLimits[(int)_spanwPhase])
        {
            if ((int)_spanwPhase >= 4)
                _spanwPhase = 0;
            else
                _spanwPhase++;
            phaseTimer = 0;

        }
        else
        {
            phaseTimer += Time.deltaTime;
        }

    }

    public void SpawnSlug(Vector3 spawnPos)
    {
        for (int i = 0; i < slugList.Count; i++)
        {
            if (!slugList[i].activeInHierarchy)
            {
                slugCount++;
                slugList[i].transform.position = spawnPos;

                slugList[i].SetActive(true);
                break;
            }
        }
    }

    public void SpawnBat(Vector3 spawnPos)
    {
        for (int i = 0; i < batList.Count; i++)
        {
            if (!batList[i].activeInHierarchy)
            {
                batCount++;
                batList[i].transform.position = spawnPos;

                batList[i].SetActive(true);
                break;
            }
        }
    }
    public void SpawnSpider(Vector3 spawnPos)
    {
        for (int i = 0; i < spiderList.Count; i++)
        {
            if (!spiderList[i].activeInHierarchy)
            {
                spiderCount++;
                spiderList[i].transform.position = spawnPos;

                spiderList[i].SetActive(true);
                break;
            }
        }
    }

    public void DespawnSlug()
    {
        for (int i = 0; i < slugList.Count; i++)
        {
            if (slugList[i].activeInHierarchy)
            {

                if (Vector3.Distance(transform.position, slugList[i].transform.position) > maxDistanceForSpawn)
                {
                    slugCount--;
                    slugList[i].SetActive(false);
                }
            }
        }
    }
    public void DespawnBat()
    {
        for (int i = 0; i < batList.Count; i++)
        {
            if (batList[i].activeInHierarchy)
            {

                if (Vector3.Distance(transform.position, batList[i].transform.position) > maxDistanceForSpawn)
                {
                    batCount--;
                    batList[i].SetActive(false);
                }
            }
        }
    }
    public void DespawnSpider()
    {
        for (int i = 0; i < spiderList.Count; i++)
        {
            if (spiderList[i].activeInHierarchy)
            {

                if (Vector3.Distance(transform.position, spiderList[i].transform.position) > maxDistanceForSpawn)
                {
                    spiderCount--;
                    spiderList[i].SetActive(false);
                }
            }
        }
    }

    Vector3 GetSpawnLoaction()
    {
        float x = Random.Range(transform.position.x - proximitySpawn, transform.position.x + proximitySpawn);
        float z = Random.Range(transform.position.z - proximitySpawn, transform.position.z + proximitySpawn);


        return new Vector3(x, 0.7f, z);
    }


}
