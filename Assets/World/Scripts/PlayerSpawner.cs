﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerSpawner : MonoBehaviour
{
    public GameObject player1;
    public GameObject player2;
    public GameObject player3;
    public GameObject player4;

    public Text spawnInfo1;
    public Text spawnInfo2;
    public Text spawnInfo3;
    public Text spawnInfo4;

    string respawnText = "Press Start to Join";


    bool canSpawn1;
    bool canSpawn2;
    bool canSpawn3;
    bool canSpawn4;

    bool hasJoined1;
    bool hasJoined2;
    bool hasJoined3;
    bool hasJoined4;

    float maxRespawnTimer;

    float respawnTimer1;
    float respawnTimer2;
    float respawnTimer3;
    float respawnTimer4;

    Vector3 cameraOffset;

    int playerCount;


    void Start()
    {
        cameraOffset = new Vector3(transform.position.x, 1, transform.position.z);
        maxRespawnTimer = 30;

        canSpawn1 = true;
        canSpawn2 = true;
        canSpawn3 = true;
        canSpawn4 = true;

        player1.SetActive(false);
        player2.SetActive(false);
        player3.SetActive(false);
        player4.SetActive(false);

        hasJoined1 = false;
        hasJoined2 = false;
        hasJoined3 = false;
        hasJoined4 = false;

        playerCount = 0;
    }

    void Update()
    {
        if (Input.GetButtonDown("StartButton1") && canSpawn1 && !player1.activeInHierarchy)
        {
            RespawnPlayer(1);
            spawnInfo1.enabled = false;
            if (!hasJoined1)
            {
                hasJoined1 = true;
                playerCount++;
            }
        }
        if (Input.GetButtonDown("StartButton2") && canSpawn2 && !player2.activeInHierarchy)
        {
            RespawnPlayer(2);
            spawnInfo2.enabled = false;
            if (!hasJoined1)
            {
                hasJoined2 = true;
                playerCount++;
            }
        }
        if (Input.GetButtonDown("StartButton3") && canSpawn3 && !player3.activeInHierarchy)
        {
            RespawnPlayer(3);
            spawnInfo3.enabled = false;
            if (!hasJoined1)
            {
                hasJoined3 = true;
                playerCount++;
            }
        }
        if (Input.GetButtonDown("StartButton4") && canSpawn4 && !player4.activeInHierarchy)
        {
            RespawnPlayer(4);
            spawnInfo4.enabled = false;
            if (!hasJoined1)
            {
                hasJoined4 = true;
                playerCount++;
            }
        }
        if (!canSpawn1)
        {
            if (respawnTimer1 <= 0.0f)
            {
                canSpawn1 = true;
                spawnInfo1.text = respawnText;
            }
            else
            {
                respawnTimer1 -= Time.deltaTime;
                spawnInfo1.text = "Respawn in " + respawnTimer1.ToString("##");
            }
        }
        if (!canSpawn2)
        {
            if (respawnTimer2 <= 0.0f)
            {
                canSpawn2 = true;
                spawnInfo2.text = respawnText;
            }
            else
            {
                respawnTimer2 -= Time.deltaTime;
                spawnInfo2.text = "Respawn in " + respawnTimer2.ToString("##");
            }
        }
        if (!canSpawn3)
        {
            if (respawnTimer3 <= 0.0f)
            {
                canSpawn3 = true;
                spawnInfo3.text = respawnText;
            }
            else
            {
                respawnTimer3 -= Time.deltaTime;
                spawnInfo3.text = "Respawn in " + respawnTimer3.ToString("##");
            }
        }
        if (!canSpawn4)
        {
            if (respawnTimer4 <= 0.0f)
            {
                canSpawn4 = true;
                spawnInfo4.text = respawnText;
            }
            else
            {
                respawnTimer4 -= Time.deltaTime;
                spawnInfo4.text = "Respawn in " + respawnTimer4.ToString("##");
            }
        }


    }

    void RespawnPlayer(int player)
    {
        GameObject respawn;
        if (player == 1)
        {
            respawn = player1;
        }
        else if (player == 2)
        {
            respawn = player2;
        }
        else if (player == 3)
        {
            respawn = player3;
        }
        else
        {
            respawn = player4;
        }
        respawn.transform.position = new Vector3(transform.position.x - cameraOffset.x, 1, transform.position.z - cameraOffset.z);
        respawn.GetComponent<PlayerHealth>().healthUpdate = 100;
        respawn.GetComponent<PlayerHealth>().dead = false;
        respawn.SetActive(true);


    }
    public void PlayerDeath(int player)
    {
        if (player == 1)
        {
            spawnInfo1.enabled = true;
            respawnTimer1 = maxRespawnTimer;
            canSpawn1 = false;
        }
        if (player == 2)
        {
            spawnInfo2.enabled = true;
            respawnTimer2 = maxRespawnTimer;
            canSpawn2 = false;
        }
        if (player == 3)
        {
            spawnInfo3.enabled = true;
            respawnTimer3 = maxRespawnTimer;
            canSpawn3 = false;
        }
        if (player == 4)
        {
            spawnInfo4.enabled = true;
            respawnTimer4 = maxRespawnTimer;
            canSpawn4 = false;
        }
    }
}
