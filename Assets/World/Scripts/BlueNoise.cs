﻿using System;

class BlueNoise
{
    public int[,] noiseVals;
    public BlueNoise(int seed)
    {
        Random rand = new Random(seed);
        noiseVals = new int[16, 16];
        for (int x = 0; x < 16; x++)
        {
            for (int z = 0; z < 16; z++)
            {
                noiseVals[x, z] = rand.Next(100);
            }
        }
    }
    public float NoiseAt(int x, int z)
    {
        int y = noiseVals[x % 16, z % 16];

        return mix(x, z, y);
    }
    float mix(int a, int b, int c)
    {
        a = a - b; a = a - c; a = a ^ (int)((uint)c >> 13);
        b = b - c; b = b - a; b = b ^ (a << 8);
        c = c - a; c = c - b; c = c ^ (int)((uint)b >> 13);
        a = a - b; a = a - c; a = a ^ (int)((uint)c >> 12);
        b = b - c; b = b - a; b = b ^ (a << 16);
        c = c - a; c = c - b; c = c ^ (int)((uint)b >> 5);
        a = a - b; a = a - c; a = a ^ (int)((uint)c >> 3);
        b = b - c; b = b - a; b = b ^ (a << 10);
        c = c - a; c = c - b; c = c ^ (int)((uint)b >> 15);
        return (Math.Abs(c) / 4294967295f);
    }
}
