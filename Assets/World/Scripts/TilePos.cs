﻿using System;
using UnityEngine;

public struct TilePos
{
    public int x;
    public int z;

    public TilePos(int xVal, int zVal)
    {
        this.x = xVal;
        this.z = zVal; 

    }

    public override int GetHashCode()
    {
        unchecked
        {
            int hash = 47;

            hash = hash * 227 + x.GetHashCode();
            hash = hash * 227 + z.GetHashCode();

            return hash;
        }
    }
    public override bool Equals(object obj)
    {
        if (GetHashCode() == obj.GetHashCode())
            return true;
        return false;
    }
}