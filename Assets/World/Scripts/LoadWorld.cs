﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoadWorld : MonoBehaviour
{

    public Tile tile1;
    public Tile tile2;
    public Tile tile3;
    public Tile tile4;
    public Tile tile5;
    public Tile tile6;

    public GameObject sunShafts;


    public Transform central;

    public int viewDistance = 1;
    public int tileSize = 10;

    TilePos currentPos;

    public int offset = 200;

    BlueNoise noise;

    public float frequency = 5f; // lower number increases frequecy of bumps heigher number reduce rate
    private float division = 0.015f;  //lower number increase range in height heigher number is flatter

    Dictionary<TilePos, Tile> _world;

    Vector3 tileOffset = new Vector3(5, 0, 5);

    // Use this for initialization
    void Start()
    {



        _world = new Dictionary<TilePos, Tile>();

        noise = new BlueNoise(1234);



        int playerChunkX = Mathf.FloorToInt(central.position.x / tileSize);
        int playerChunkZ = Mathf.FloorToInt(central.position.z / tileSize);
        currentPos = new TilePos();
        currentPos.x = playerChunkX;
        currentPos.z = playerChunkZ;


        CreateTile(playerChunkX , playerChunkZ );

        for (int x = (viewDistance * -1); x < viewDistance + 1; x++)
        {
            for (int z = (viewDistance * -1); z < viewDistance + 1; z++)
            {
                if (x == 0 & z == 0)
                    continue;
                CreateTile(playerChunkX + x, playerChunkZ + z);
            }
        }


    }
    void CreateTile(int x, int z)
    {
        Tile t;
        if (_world.TryGetValue(new TilePos(x, z),out t))
        {
            return;
        }


        var weight = GetWeight(x, z);
        //print(weight);
        var rotation = weight %4;

        if (rotation >3)
        {
            rotation = 270;
        }
        else if (rotation > 2)
        {
            rotation = 180;
        }
        else if (rotation > 1)
        {
            rotation = 90;
        }
        else 
        {
            rotation = 0;
        }

        Tile go;
        GameObject sunshafts;
        if (weight < 25)
        {
            go = Instantiate(tile2) as Tile; //POI 1
        }
        else if (weight < 30)
        {
            go = Instantiate(tile3) as Tile;//POI 2
        }
        else if (weight < 32)
        {
            go = Instantiate(tile4) as Tile; //POI 3
        }
        else if (weight < 34)
        {
            go = Instantiate(tile5) as Tile; //POI 4

        }
        else if (weight < 36  && weight < 36)
        {
            go = Instantiate(tile6) as Tile; //rare summon circle
        }
        else
        {
            go = Instantiate(tile1) as Tile; //common blank land
        }
        go.transform.position = new Vector3(x * tileSize, 0.1f, z * tileSize);
        
        go.transform.rotation = Quaternion.Euler(new Vector3(0, rotation, 0));
        if (weight > 26 &&weight < 34)
        {
            sunshafts = Instantiate(sunShafts);
            sunshafts.transform.parent = go.transform;
            sunshafts.transform.localPosition = Vector3.zero;
        }
        go.transform.parent = this.transform;
        _world.Add(new TilePos(x, z), go);
    }
    // Update is called once per frame
    void Update()
    {
        //transition between "chunks" load and unload
        int playerChunkX = Mathf.FloorToInt(central.position.x / tileSize);
        int playerChunkZ = Mathf.FloorToInt(central.position.z / tileSize);

        if (playerChunkX != currentPos.x || playerChunkZ != currentPos.z)
        {
            //unload tiles
            UnloadTiles(playerChunkX, playerChunkZ);
            //load tiles
            GenerateTile(playerChunkX, playerChunkZ);

        }


    }

    void UnloadTiles(int playerX, int playerZ)
    {
        int offsetX = playerX - currentPos.x;
        int offsetZ = playerZ - currentPos.z;

        Tile outTile;

        if (offsetX > 0) //positive x transition
        {
            for (int z = (viewDistance * -1); z < viewDistance + 1; z++)
            {
                TilePos t = new TilePos(currentPos.x -(viewDistance * offsetX), playerZ + z -offsetZ);
                if (_world.TryGetValue(t, out outTile))
                {
                    _world.Remove(t);
                    Destroy(outTile.gameObject);
                }
            }
        }
        else if (offsetX < 0)//negative x transition
        {
            for (int z = (viewDistance * -1); z < viewDistance + 1; z++)
            {
                TilePos t = new TilePos(currentPos.x - (viewDistance * offsetX), playerZ + z - offsetZ);
                if (_world.TryGetValue(t, out outTile))
                {
                    _world.Remove(t);
                    Destroy(outTile.gameObject);
                }
            }
        }
        if (offsetZ > 0) //positive Z transition
        {
            for (int x = (viewDistance * -1); x < viewDistance + 1; x++)
            {
                TilePos t = new TilePos(playerX + x -offsetX, currentPos.z - (viewDistance * offsetZ));
                if (_world.TryGetValue(t, out outTile))
                {
                    _world.Remove(t);
                    Destroy(outTile.gameObject);
                }
            }
        }
        else if (offsetZ < 0)//negative z transition
        {
            for (int x = (viewDistance * -1); x < viewDistance + 1; x++)
            {
                TilePos t = new TilePos(playerX + x - offsetX, currentPos.z - (viewDistance * offsetZ));
                if (_world.TryGetValue(t, out outTile))
                {
                    _world.Remove(t);
                    Destroy(outTile.gameObject);
                }
            }
        }


    }

    void GenerateTile(int playerX, int playerZ)
    {
        int offsetX = playerX - currentPos.x;
        int offsetZ = playerZ - currentPos.z;

        if (offsetX > 0) //positive x transition
        {
            for (int z = (viewDistance * -1); z < viewDistance + 1; z++)
            {
                TilePos t = new TilePos(playerX + (viewDistance * offsetX), playerZ + z);
                CreateTile(t.x, t.z);
            }
        }
        else if (offsetX < 0)//negative x transition
        {
            for (int z = (viewDistance * -1); z < viewDistance + 1; z++)
            {
                TilePos t = new TilePos(playerX + (viewDistance * offsetX), playerZ + z);
                CreateTile(t.x, t.z);
            }
        }
        if (offsetZ > 0) //positive Z transition
        {
            for (int x = (viewDistance * -1); x < viewDistance + 1; x++)
            {
                TilePos t = new TilePos(playerX +x, playerZ + (viewDistance * offsetZ));
                CreateTile(t.x, t.z);
            }
        }
        else if (offsetZ < 0)//negative z transition
        {
            for (int x = (viewDistance * -1); x < viewDistance + 1; x++)
            {
                TilePos t = new TilePos(playerX + x, playerZ + (viewDistance * offsetZ));
                CreateTile(t.x, t.z);
            }
        }
        currentPos.x = playerX;
        currentPos.z = playerZ;
    }

    float GetWeight(int x, int z)
    {
        //x += Mathf.RoundToInt(central.position.x);
        //z += Mathf.RoundToInt(central.position.z);
        //float perlin1 = (Mathf.PerlinNoise(x / frequency, z / frequency)) / division;
        x = Mathf.Abs(x + offset);
        z = Mathf.Abs(z + offset);

        var weight = noise.NoiseAt(x, z) * 100;

        return weight;

    }
}
