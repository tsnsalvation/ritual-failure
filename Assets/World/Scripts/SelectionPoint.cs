﻿using UnityEngine;
using System.Collections;

public class SelectionPoint : MonoBehaviour {

	public SummoningCircle _circleScript;
	public bool hasPlayer;

	public enum PointDirection
	{
		North,
		East,
		West,
		South,
	}
	public GameObject[] SpawnList;
	public GameObject partToSpawn;
	public PointDirection whichPoint;
	public GameObject activeplayer;
	public bool MiniGameActive;
	public bool MiniGameCanStart;

	PlayerController _playerscript;
	GameObject spiningCircle;

	public bool InInputArea;
	GameObject displayObject;

    bool soundPlayed;

    public AudioSource selectionSound;
    //	IList<GameObject> playerList;
    // Use this for initialization
    void Start () {
//		playerList = GameObject.FindGameObjectsWithTag ("Player");
		_circleScript = this.gameObject.transform.parent.GetComponent<SummoningCircle>();

	
	}
	
	// Update is called once per frame
	void Update () {
		if (hasPlayer) 
		{
			ChoosePart ();
		}

		if (partToSpawn != displayObject) 
		{

		}
	}



	void ChoosePart()
	{
		if (_playerscript.inputA == true || _playerscript.inputB == true  || _playerscript.inputX == true  ||_playerscript.inputY == true ) 
		{
            if (!soundPlayed)
            {
                selectionSound.Play();
                soundPlayed = true;
            }

            if (_playerscript.inputA == true) 
			{
				partToSpawn =SpawnList [0];
			}
			else if (_playerscript.inputB == true) 
			{
				partToSpawn =SpawnList [1];
			}
			else if (_playerscript.inputX == true) 
			{
				partToSpawn = SpawnList [2];
			}
			else if (_playerscript.inputY == true) 
			{
				partToSpawn = SpawnList [3];
			}


		}
	}

	public void ActivateMiniGame()
	{
		switch (whichPoint) 
		{
		case PointDirection.North:
			MiniGameCanStart = false;
			MiniGameActive = true;
//			SpinTheCircle ();
			break;
		case PointDirection.East:
			MiniGameCanStart = false;
			MiniGameActive = true;
			break;
		case PointDirection.West: 
			MiniGameCanStart = false;
			MiniGameActive = true;
			break;
		case PointDirection.South:
			MiniGameCanStart = false;
			MiniGameActive = true;
			break;

		}

	}

	void SpinTheCircle()
	{
		int attempts = 0;

		spiningCircle = this.gameObject.transform.GetChild (0).gameObject;

		spiningCircle.SetActive (true);

		spiningCircle.GetComponent<Animation> ().Play ();

//		if (this.gameObject.GetComponent<BoxCollider> ().bounds.Contains (spiningCircle.transform.position)) {
//			InInputArea = true;
//		} else 
//		{
//			InInputArea = false;
//		}

		if (_playerscript.inputA == true || _playerscript.inputB == true || _playerscript.inputX == true || _playerscript.inputY == true) 
		{
			if (InInputArea) {
				spiningCircle.GetComponent<Animation> ().Play ();
				attempts += 1;

			} 
//			else 
//			{
//				FailMiniGame ();
//			}
		}
	}
	void FailMiniGame()
	{
		MiniGameActive = false;
		spiningCircle.GetComponent<Animation> ().Stop ();
		spiningCircle.SetActive (true);


	}

//	

	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "Player")
		{
			hasPlayer = true;
			MiniGameCanStart = true;
			activeplayer = col.gameObject;
			_playerscript = col.gameObject.GetComponent<PlayerController> ();

//			case PointDirection.East:
//				_circleScript.summoner[1] = col.gameObject;
//				break;
//			case PointDirection.West: 
//				_circleScript.summoner[2] = col.gameObject;
//				break;
//			case PointDirection.South:
//				_circleScript.summoner[3] = col.gameObject;
//				break;
//
//			}
		}
		if (col.gameObject == spiningCircle)
		{
			InInputArea = true;
		}


	}

	void OnTriggerExit(Collider col)
	{
		if(col.tag == "Player" && !_circleScript.ActiveCircle)
		{
			hasPlayer = false;
			MiniGameCanStart = false;
			activeplayer = null;
			_playerscript = null;
////
////			switch (whichPoint) 
////			{
////			case PointDirection.North:
////				_circleScript.summoner[0] = null;
////				break;
////			case PointDirection.East:
////				_circleScript.summoner[1] = null;
////				break;
////			case PointDirection.West: 
////				_circleScript.summoner[2] = null;
////				break;
////			case PointDirection.South:
////				_circleScript.summoner[3] = null;
////				break;
//
//			}
		}

		if (col.gameObject == spiningCircle)
		{
			InInputArea = false;
		}

	}
}

